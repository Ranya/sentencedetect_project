package com.example.testOpenNLP;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class PartsOfSpeech {
	String sentence;
	public void givenEnglishModel_partsOfSpeech() 
			  throws IOException,InvalidFormatException {
		
		//définir des variables pour les langages trainées
		InputStream tokenModelIn = null;
	    InputStream posModelIn = null;  
	    
	    //instatiation de classe TestSentenceDetect
		//TestSentenceDetect detect=new TestSentenceDetect();
		
		//charger le fichier "en-token.bin"
		tokenModelIn = new FileInputStream("src/main/resources/models/en-token.bin");
		
		// Parts-Of-Speech Tagging
        // reading parts-of-speech model to a stream
	    posModelIn = new FileInputStream("src/main/resources/models/en-pos-maxent.bin");
		
		//appel de la méthode de découpage de paragraphe
		//detect.givenEnglishModel_devideParagrapgh();
		
		
		 /*for(int i=0;i<detect.getSentences().size();i++){
	            //System.out.println(detect.getSentences()[i]);
			 sentence = detect.getSentences().elementAt(i);
	        }*/
		 
	    
	    //String sentence = " How old is John? John is 27 years.";
		Scanner myInput = new Scanner(System.in);
		System.out.println("Type your paragraph in English: ");
		 
		String sentence = myInput.nextLine();
	    
	    // tokenize the sentence
	    TokenizerModel tokenModel = new TokenizerModel(tokenModelIn);
	    Tokenizer tokenizer = new TokenizerME(tokenModel);
	    String tokens[] = tokenizer.tokenize(sentence);
	    
	    
        // loading the parts-of-speech model from stream
        POSModel posModel = new POSModel(posModelIn);
        
        // initializing the parts-of-speech tagger with model
        POSTaggerME posTagger = new POSTaggerME(posModel);
        
        // Tagger tagging the tokens
        String tags[] = posTagger.tag(tokens);
        
        // Getting the probabilities of the tags given to the tokens
        double probs[] = posTagger.probs();
		
        System.out.println("Token\t:\tTag\t:\tProbability\n---------------------------------------------");
        for(int j=0;j<tokens.length;j++){
            System.out.println(tokens[j]+"\t:\t"+tags[j]+"\t:\t"+probs[j]);
        }
        
	}
	
}
