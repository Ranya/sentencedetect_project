package com.example.testOpenNLP;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

@SpringBootApplication
public class TestOpenNlpApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(TestOpenNlpApplication.class, args);
		
		TestSentenceDetect detect=new TestSentenceDetect();
		PartsOfSpeech detectparts = new PartsOfSpeech();
		
		/*try {
			detect.givenEnglishModel_devideParagrapgh();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		try {
			detectparts.givenEnglishModel_partsOfSpeech();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
