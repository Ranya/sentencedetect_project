package com.example.testOpenNLP;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.Vector;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

public class TestSentenceDetect {
	 Vector<String> sentences;
	 
	public Vector<String> getSentences() {
		return sentences;
	}

	public void setSentences(Vector<String> sentences) {
		this.sentences = sentences;
	}

	public void givenEnglishModel_devideParagrapgh() 
			  throws IOException,InvalidFormatException {
	        
		 //String sentences[];
		 Scanner myInput = new Scanner(System.in);
		 System.out.println("Type your paragraph in English : ");
		 
		 String paragraph = myInput.nextLine();
		 
	    /*String paragraph = "This is a statement. This is another statement."
	      + "Now is an abstract word for time, "
	      + "that is always flying. And my email address is google@gmail.com.";*/
	 
		 
		//Charger le fichier .bin
	    InputStream is = getClass().getResourceAsStream("/models/en-sent.bin");
	    //créer le modele avec la language trainé en-sent.bin
	    SentenceModel model = new SentenceModel(is);
	    //methode pré-definie pour décomposer une paragraphe sous des phrases.
	    SentenceDetectorME sdetector = new SentenceDetectorME(model);
	    
	    //permet de stocker chaque ligne dans un tableau nommé "sentences".
	    String sentensesTable[] = sdetector.sentDetect(paragraph);
	    for(int i=0;i<sentensesTable.length;i++){
	    	sentences.addElement(sentensesTable[i]);
        }
	    
	    
	   /*for(int i=0;i<sentences.length;i++){
            System.out.println(sentensesTable[i]);
        }*/
	    
	    
        //is.close();
	    /*assertThat(sentences).contains(
	      "This is a statement.",
	      "This is another statement.",
	      "Now is an abstract word for time, that is always flying.",
	      "And my email address is google@gmail.com.");*/	}

}